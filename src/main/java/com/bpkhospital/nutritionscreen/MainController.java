package com.bpkhospital.nutritionscreen;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;

import javax.validation.*;

import com.bpkhospital.nutritionscreen.Record;
import com.bpkhospital.nutritionscreen.RecordReqDTO;
import com.bpkhospital.nutritionscreen.RecordRepository;
import com.bpkhospital.nutritionscreen.ApiLogger;
import com.bpkhospital.nutritionscreen.security.Authorization;

@Controller    
@RequestMapping(path="/api/records") 
public class MainController {
	@Autowired 
	private RecordRepository recordRepository;
        
        private Authorization auth = new Authorization();
        
        private ApiLogger apiLogger = new ApiLogger();
        
        @Value("${PostRecordLogging}")
        private String PostRecordLogging;
        
//----- Methods for Create -------------------
	@PostMapping() 
        public ResponseEntity addNewRecord (@RequestHeader(value = "Authorization") String token,
                @Valid @RequestBody RecordReqDTO reqBody) {
            
            // API Logging -----------------------------
            if (PostRecordLogging.equals("ON"))
            {
                apiLogger.writePostRecords(reqBody);
            }
            // End of API Logging -----------------------------

            // Authorization----------------------------
            if (!auth.isAuthorized(token))
            {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            // end of Authorization----------------------------

            // Map request body to data model-----------------------------
            Record newRecord = new Record();

            newRecord.setStaffId(reqBody.getStaffId());
            newRecord.setStaffName(reqBody.getStaffName());
            newRecord.setScreeningDate(reqBody.getScreeningDate());
            newRecord.setScreeningTime(reqBody.getScreeningTime());
            newRecord.setPatientName(reqBody.getPatientName());
            newRecord.setGender(reqBody.getGender().toString());
            newRecord.setAge(reqBody.getAge());
            newRecord.setHn(reqBody.getHn());
            newRecord.setVn(reqBody.getVn());
            newRecord.setAn(reqBody.getAn());
            newRecord.setAdmitDate(reqBody.getAdmitDate());
            newRecord.setAdmitTime(reqBody.getAdmitTime());
            newRecord.setInitialDiagnosis(reqBody.getInitialDiagnosis());
            newRecord.setDataSource(reqBody.getDataSource().toString());
            newRecord.setOtherSource(reqBody.getOtherSource());
            newRecord.setHeight(reqBody.getHeight());
            newRecord.setBodyLength(reqBody.getBodyLength());
            newRecord.setArmSpan(reqBody.getArmSpan());
            newRecord.setHeightFromRelatives(reqBody.getHeightFromRelatives());
            newRecord.setWeight(reqBody.getWeight());
            newRecord.setWeighMethod(reqBody.getWeighMethod().toString());
            newRecord.setBmi(reqBody.getBmi());
            newRecord.setAlbumin(reqBody.getAlbumin());
            newRecord.setTlc(reqBody.getTlc());
            newRecord.setBodyShape(reqBody.getBodyShape().toString());
            newRecord.setWeightChange4w(reqBody.getWeightChange4w().toString());
            newRecord.setFoodType2w(reqBody.getFoodType2w().toString());
            newRecord.setConsumption2w(reqBody.getConsumption2w().toString());
            
            // Convert array members of ChewSwallowProblem into single string
            RecordReqDTO.enumChewSwallowProblem [] listChewSwallowProblem = reqBody.getChewSwallowProblem();
            String strChewSwallowProblem = "";
            
            for (int i = 0; i< listChewSwallowProblem.length; i++)
            {
               
               if (i < (listChewSwallowProblem.length-1))
               {
                   strChewSwallowProblem += listChewSwallowProblem[i].toString() + ",";
               }
               else
               {
                   strChewSwallowProblem += listChewSwallowProblem[i].toString();
               }
            }
//            System.out.println(strChewSwallowProblem);
            // Map ChewSwallowProblem data from request body to data model 
            newRecord.setChewSwallowProblem(strChewSwallowProblem);
            
           
            // Convert value of array members of AlimentaryProblem into single string
            RecordReqDTO.enumAlimentaryProblem [] listAlimentaryProblem = reqBody.getAlimentaryProblem();
            String strAlimentaryProblem = "";
            
            for (int i = 0; i< listAlimentaryProblem.length; i++)
            {
               if (i < (listAlimentaryProblem.length-1))
               {
                   strAlimentaryProblem += listAlimentaryProblem[i].toString() + ",";
               }
               else
               {
                   strAlimentaryProblem += listAlimentaryProblem[i].toString();
               }
            }
//            System.out.println(strAlimentaryProblem);
            // Map AlimentaryProblem data from request body to data model
            newRecord.setAlimentaryProblem(strAlimentaryProblem);
            
            
            // Convert value of array members of EatProblem into single string
            RecordReqDTO.enumEatProblem [] listEatProblem = reqBody.getEatProblem();
            String strEatProblem = "";
            
            for (int i = 0; i< listEatProblem.length; i++)
            {
               if (i < (listEatProblem.length-1))
               {
                   strEatProblem += listEatProblem[i].toString() + ",";
               }
               else
               {
                   strEatProblem += listEatProblem[i].toString();
               }
            }
//            System.out.println(strEatProblem);
            // Map EatProblem data from request body to data model
            newRecord.setEatProblem(strEatProblem);
             
            newRecord.setFoodAccessibility(reqBody.getFoodAccessibility().toString());

            
            // Convert value of array members of Disease into single string
            RecordReqDTO.enumDisease [] listDisease = reqBody.getDisease();
            String strDisease = "";
            
            for (int i = 0; i< listDisease.length; i++)
            {
               if (i < (listDisease.length-1))
               {
                   strDisease += listDisease[i].toString() + ",";
               }
               else
               {
                   strDisease += listDisease[i].toString();
               }
            }
//            System.out.println(strDisease);
            // Map Disease data from request body to data model
            newRecord.setDisease(strDisease);
            
            newRecord.setScoreItem2(reqBody.getScoreItem2());
            newRecord.setScoreItem3(reqBody.getScoreItem3());
            newRecord.setScoreItem4(reqBody.getScoreItem4());
            newRecord.setScoreItem5(reqBody.getScoreItem5());
            newRecord.setScoreItem6(reqBody.getScoreItem6());
            newRecord.setScoreItem7(reqBody.getScoreItem7());
            newRecord.setScoreItem8(reqBody.getScoreItem8());
            newRecord.setTotalScore(reqBody.getTotalScore());
            
            // End Map request body to data model-----------------------------
  
            // Save to database
            recordRepository.save(newRecord);
            
            //Send API response - return id of new record
            ResCreateSuccess responseBody = new ResCreateSuccess(newRecord.getId());
            return new ResponseEntity(responseBody, HttpStatus.CREATED);
	}
        
       
//------ End Methods for Create -------------------
        
//----- Methods for Read -------------------      
	// Read All records
        @GetMapping()
        public ResponseEntity getAllRecords(@RequestHeader(value = "Authorization") String token) {
            
            // Authorization----------------------------
            if (!auth.isAuthorized(token))
            {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            
            // end of Authorization----------------------------
            
            Iterable<Record> responseBody = recordRepository.findAll();
             
            if (responseBody.iterator().hasNext())
            {
                return new ResponseEntity(responseBody,HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
                
	}
        
        // Read one record
        @GetMapping(value="/{recordId}")
        public ResponseEntity getUser(@RequestHeader(value = "Authorization") String token, 
                @PathVariable("recordId") Long recordId) {
            
            // Authorization----------------------------
            if (!auth.isAuthorized(token))
            {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            
            // end of Authorization----------------------------
            
            Record responseBody = recordRepository.findOne(recordId);
            
            if (responseBody != null)
            {
                return new ResponseEntity(responseBody,HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            
        }
        
//------ End Methods for Read -------------------        
        
}


//class for response body of Create success -> return record id
class ResCreateSuccess 
{
    private final Long recordId;
    public ResCreateSuccess(Long id)
    {
        this.recordId = id;
    }
    
    public Long getRecordId()
    {
        return recordId;
    }
}
