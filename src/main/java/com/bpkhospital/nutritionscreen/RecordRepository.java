package com.bpkhospital.nutritionscreen;

import org.springframework.data.repository.CrudRepository;

import com.bpkhospital.nutritionscreen.Record;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface RecordRepository extends CrudRepository<Record, Long> {

}
