package com.bpkhospital.nutritionscreen;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;
import java.io.*; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bpkhospital.nutritionscreen.RecordReqDTO;

public class ApiLogger{
    
    static final Logger LOG = LoggerFactory.getLogger(ApiLogger.class); // For spring.log writing if failed to write API log
    private ObjectMapper mapper = new ObjectMapper(); // For object to json 
    
    public void writePostRecords(RecordReqDTO reqBody)
    {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String strDate1 = dateFormat.format(date); // For file name
        String strDate2 = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date); // For timestamp
        
         try 
       {
           String strReqBody = mapper.writeValueAsString(reqBody);
           
           File file = new File("./logs/ApiPostRecords_"+strDate1+".log");
           
           FileWriter writer = new FileWriter(file, true);
           
           writer.write(strDate2 + " " +strReqBody +"\n");
           writer.close();           

       }
       catch (Exception e)
       {
           LOG.error("API Logging error: "+ e);
       }
        
    }
            
}
