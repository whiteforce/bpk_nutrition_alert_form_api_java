package com.bpkhospital.nutritionscreen.security;
 
public class Authorization {
    
        private String accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBbmRyb2lkIiwiaXNzIjoiYnBrIn0.PhbTIIboNjoaanOi39C27ReXf0DemnX3wpc263tdZwA";

        public boolean isAuthorized(String token)
        {
            if (token == null || !token.equals(accessToken))
            {
                return false;
            }
            return true;
        }
        
        public String getAccessToken()
        {
            return accessToken;
        }
    
}