package com.bpkhospital.nutritionscreen;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Record {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

        private String staffId;
        private String staffName;
        private String screeningDate;
        private String screeningTime;
        private String patientName;
        private String gender;
        private int age;
        private String hn;
        private String vn;
        private String an;
        private String admitDate;
        private String admitTime;
        private String initialDiagnosis;
        private String dataSource;
        private String otherSource;
        private int height;
        private int bodyLength;
        private int armSpan;
        private int heightFromRelatives;
        private int weight;
        private String weighMethod;
        private double bmi;
        private double albumin;
        private double tlc;
        private String bodyShape;
        private String weightChange4w;
        private String foodType2w;
        private String consumption2w;
        private String chewSwallowProblem;
        private String alimentaryProblem;
        private String eatProblem;
        private String foodAccessibility;
        private String disease;
        private int scoreItem2;
        private int scoreItem3;
        private int scoreItem4;
        private int scoreItem5;
        private int scoreItem6;
        private int scoreItem7;
        private int scoreItem8;
        private int totalScore;

        
        public Long getId() {
		return id;
	}
        
        public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
        
        public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
        
        public String getScreeningDate() {
		return screeningDate;
	}

	public void setScreeningDate(String screeningDate) {
		this.screeningDate = screeningDate;
	}
        
        public String getScreeningTime() {
		return screeningTime;
	}

	public void setScreeningTime(String screeningTime) {
		this.screeningTime = screeningTime;
	}
        
        public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
        
        public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
        
        public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
        
        public String getHn() {
		return hn;
	}

	public void setHn(String hn) {
		this.hn = hn;
	}
        
        public String getVn() {
		return vn;
	}

	public void setVn(String vn) {
		this.vn = vn;
	}
        
        public String getAn() {
		return an;
	}

	public void setAn(String an) {
		this.an = an;
	}
        
        public String getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}
        
        public String getAdmitTime() {
		return admitTime;
	}

	public void setAdmitTime(String admitTime) {
		this.admitTime = admitTime;
	}
        
        public String getInitialDiagnosis() {
		return initialDiagnosis;
	}

	public void setInitialDiagnosis(String initialDiagnosis) {
		this.initialDiagnosis = initialDiagnosis;
	}
        
        public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
        
        public String getOtherSource() {
		return otherSource;
	}

	public void setOtherSource(String otherSource) {
		this.otherSource = otherSource;
	}
        
        public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
        
        public int getBodyLength() {
		return bodyLength;
	}

	public void setBodyLength(int bodyLength) {
		this.bodyLength = bodyLength;
	}
        
        public int getArmSpan() {
		return armSpan;
	}

	public void setArmSpan(int armSpan) {
		this.armSpan = armSpan;
	}
        
        public int getHeightFromRelatives() {
		return heightFromRelatives;
	}

	public void setHeightFromRelatives(int heightFromRelatives) {
		this.heightFromRelatives = heightFromRelatives;
	}
        
        public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
        
        public String getWeighMethod() {
		return weighMethod;
	}

	public void setWeighMethod(String weighMethod) {
		this.weighMethod = weighMethod;
	}
        
        public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
        
        public double getAlbumin() {
		return albumin;
	}

	public void setAlbumin(double albumin) {
		this.albumin = albumin;
	}
        
        public double getTlc() {
		return tlc;
	}

	public void setTlc(double tlc) {
		this.tlc = tlc;
	}
        
        public String getBodyShape() {
		return bodyShape;
	}

	public void setBodyShape(String bodyShape) {
		this.bodyShape = bodyShape;
	}
        
        public String getWeightChange4w() {
		return weightChange4w;
	}

	public void setWeightChange4w(String weightChange4w) {
		this.weightChange4w = weightChange4w;
	}
        
        public String getFoodType2w() {
		return foodType2w;
	}

	public void setFoodType2w(String foodType2w) {
		this.foodType2w = foodType2w;
	}
        
        public String getConsumption2w() {
		return consumption2w;
	}

	public void setConsumption2w(String consumption2w) {
		this.consumption2w = consumption2w;
	}
        
        public String getChewSwallowProblem() {
		return chewSwallowProblem;
	}

	public void setChewSwallowProblem(String chewSwallowProblem) {
		this.chewSwallowProblem = chewSwallowProblem;
	}
        
        public String getAlimentaryProblem() {
		return alimentaryProblem;
	}

	public void setAlimentaryProblem(String alimentaryProblem) {
		this.alimentaryProblem = alimentaryProblem;
	}
        
        public String getEatProblem() {
		return eatProblem;
	}

	public void setEatProblem(String eatProblem) {
		this.eatProblem = eatProblem;
	}
        
        public String getFoodAccessibility() {
		return foodAccessibility;
	}

	public void setFoodAccessibility(String foodAccessibility) {
		this.foodAccessibility = foodAccessibility;
	}
        
        public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}
        
        public int getScoreItem2() {
		return scoreItem2;
	}

	public void setScoreItem2(int scoreItem2) {
		this.scoreItem2 = scoreItem2;
	}
        
        public int getScoreItem3() {
		return scoreItem3;
	}

	public void setScoreItem3(int scoreItem3) {
		this.scoreItem3 = scoreItem3;
	}
        
        public int getScoreItem4() {
		return scoreItem4;
	}

	public void setScoreItem4(int scoreItem4) {
		this.scoreItem4 = scoreItem4;
	}
        
        public int getScoreItem5() {
		return scoreItem5;
	}

	public void setScoreItem5(int scoreItem5) {
		this.scoreItem5 = scoreItem5;
	}
        
        public int getScoreItem6() {
		return scoreItem6;
	}

	public void setScoreItem6(int scoreItem6) {
		this.scoreItem6 = scoreItem6;
	}
        
        public int getScoreItem7() {
		return scoreItem7;
	}

	public void setScoreItem7(int scoreItem7) {
		this.scoreItem7 = scoreItem7;
	}
        
        public int getScoreItem8() {
		return scoreItem8;
	}

	public void setScoreItem8(int scoreItem8) {
		this.scoreItem8 = scoreItem8;
	}
        
        public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
        
        	
}
