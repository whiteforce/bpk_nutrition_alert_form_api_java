package com.bpkhospital.nutritionscreen;

import javax.validation.constraints.*;

public class RecordReqDTO {

//--Enum List--------------------------------    
    public static enum enumGender {
        ชาย,
        หญิง
    }
    
    public static enum enumDataSource {
        HIS,
        ผู้ป่วย,
        ญาติ,
        แหล่งข้อมูลอื่น
    }
    
    public static enum enumWeighMethod {
        ชั่งในท่านอน,
        ชั่งในท่ายืน,
        ชั่งไม่ได้,
        ญาติบอก
    }
    
    public static enum enumBodyShape {
        ผอมมาก,
        ผอม,
        อ้วนมาก,
        ปกติ_อ้วนปานกลาง
    }
    
    public static enum enumWeightChange4w {
        ลดลง_ผอมลง,
        เพิ่มขึ้น_อ้วนขึ้น,
        ไม่ทราบ,
        คงเดิม
    }
    
    public static enum enumFoodType2w {
        อาหารน้ำๆ,
        อาหารเหลวๆ,
        อาหารนุ่มกว่าปกติ,
        อาหารเหมือนปกติ
    }
    
    public static enum enumConsumption2w {
        กินน้อยมาก,
        กินน้อยลง,
        กินมากขึ้น,
        กินเท่าปกติ
    }
    
    public static enum enumChewSwallowProblem {
        สำลัก,
        เคี้ยว_กลืนลำบาก_ได้อาหารทางสาย,
        กลืนได้ปกติ
    }
    public static enum enumAlimentaryProblem {
        ท้องเสีย,
        ปวดท้อง,
        ปกติ
    }
    public static enum enumEatProblem {
        อาเจียน,
        คลื่นไส้,
        ปกติ
    }
    public static enum enumFoodAccessibility {
        นอนติดเตียง,
        ต้องมีผู้ช่วยบ้าง,
        นั่งๆ_นอนๆ,ปกติ
    }
    public static enum enumDisease {
        DM,
        CKD_ESRD,
        CLD_Cirrhosis_Hepatic_encephalopathy,
        Solid_cancer,
        Chronic_heart_failure,
        Severe_head_injury,
        Hip_fracture,
        COPD,
        two_degree_of_burn_or_above,
        Stroke_CVA,
        Septicemia,
        Severe_pneumonia,
        Multiple_fracture,
        Malignant_hematologic_disease_Bone_marrow_transplant,
        Critically_ill
    }
 //--End of Enum List--------------------------------    
    
    
	private Long id;
        
        @NotNull
        private String staffId;
        
        @NotNull
        private String staffName;
        
        @NotNull @Pattern(regexp = "[0-9]{4}-[0-1][0-9]-[0-3][0-9]")
        private String screeningDate;
        
        @NotNull @Pattern(regexp = "[0-2][0-9]:[0-5][0-9]:[0-5][0-9]")
        private String screeningTime;
        
        @NotNull
        private String patientName;
        
        @NotNull
        private enumGender gender;
        
        private int age;
        
        @NotNull
        private String hn;
        
        private String vn;
        private String an;
        
        @Pattern(regexp = "[0-9]{4}-[0-1][0-9]-[0-3][0-9]")
        private String admitDate;
        
        @Pattern(regexp = "[0-2][0-9]:[0-5][0-9]:[0-5][0-9]")
        private String admitTime;
        
        private String initialDiagnosis;
        
        @NotNull
        private enumDataSource dataSource;
        
        private String otherSource;
        private int height;
        private int bodyLength;
        private int armSpan;
        private int heightFromRelatives;
        private int weight;
        private enumWeighMethod weighMethod;
        private double bmi;
        private double albumin;
        private double tlc;
        private enumBodyShape bodyShape;
        private enumWeightChange4w weightChange4w;
        private enumFoodType2w foodType2w;
        private enumConsumption2w consumption2w;
        private enumChewSwallowProblem [] chewSwallowProblem;
        private enumAlimentaryProblem [] alimentaryProblem;
        private enumEatProblem [] eatProblem;
        private enumFoodAccessibility foodAccessibility;
        private enumDisease [] disease;
        private int scoreItem2;
        private int scoreItem3;
        private int scoreItem4;
        private int scoreItem5;
        private int scoreItem6;
        private int scoreItem7;
        private int scoreItem8;
        private int totalScore;

        
        public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
        
        public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
        
        public String getScreeningDate() {
		return screeningDate;
	}

	public void setScreeningDate(String screeningDate) {
		this.screeningDate = screeningDate;
	}
        
        public String getScreeningTime() {
		return screeningTime;
	}

	public void setScreeningTime(String screeningTime) {
		this.screeningTime = screeningTime;
	}
        
        public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
        
        public enumGender getGender() {
		return gender;
	}

	public void setGender(enumGender gender) {
		this.gender = gender;
	}
        
        public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
        
        public String getHn() {
		return hn;
	}

	public void setHn(String hn) {
		this.hn = hn;
	}
        
        public String getVn() {
		return vn;
	}

	public void setVn(String vn) {
		this.vn = vn;
	}
        
        public String getAn() {
		return an;
	}

	public void setAn(String an) {
		this.an = an;
	}
        
        public String getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}
        
        public String getAdmitTime() {
		return admitTime;
	}

	public void setAdmitTime(String admitTime) {
		this.admitTime = admitTime;
	}
        
        public String getInitialDiagnosis() {
		return initialDiagnosis;
	}

	public void setInitialDiagnosis(String initialDiagnosis) {
		this.initialDiagnosis = initialDiagnosis;
	}
        
        public enumDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(enumDataSource dataSource) {
		this.dataSource = dataSource;
	}
        
        public String getOtherSource() {
		return otherSource;
	}

	public void setOtherSource(String otherSource) {
		this.otherSource = otherSource;
	}
        
        public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
        
        public int getBodyLength() {
		return bodyLength;
	}

	public void setBodyLength(int bodyLength) {
		this.bodyLength = bodyLength;
	}
        
        public int getArmSpan() {
		return armSpan;
	}

	public void setArmSpan(int armSpan) {
		this.armSpan = armSpan;
	}
        
        public int getHeightFromRelatives() {
		return heightFromRelatives;
	}

	public void setHeightFromRelatives(int heightFromRelatives) {
		this.heightFromRelatives = heightFromRelatives;
	}
        
        public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
        
        public enumWeighMethod getWeighMethod() {
		return weighMethod;
	}

	public void setWeighMethod(enumWeighMethod weighMethod) {
		this.weighMethod = weighMethod;
	}
        
        public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
        
        public double getAlbumin() {
		return albumin;
	}

	public void setAlbumin(double albumin) {
		this.albumin = albumin;
	}
        
        public double getTlc() {
		return tlc;
	}

	public void setTlc(double tlc) {
		this.tlc = tlc;
	}
        
        public enumBodyShape getBodyShape() {
		return bodyShape;
	}

	public void setBodyShape(enumBodyShape bodyShape) {
		this.bodyShape = bodyShape;
	}
        
        public enumWeightChange4w getWeightChange4w() {
		return weightChange4w;
	}

	public void setWeightChange4w(enumWeightChange4w weightChange4w) {
		this.weightChange4w = weightChange4w;
	}
        
        public enumFoodType2w getFoodType2w() {
		return foodType2w;
	}

	public void setFoodType2w(enumFoodType2w foodType2w) {
		this.foodType2w = foodType2w;
	}
        
        public enumConsumption2w getConsumption2w() {
		return consumption2w;
	}

	public void setConsumption2w(enumConsumption2w consumption2w) {
		this.consumption2w = consumption2w;
	}
        
        public enumChewSwallowProblem [] getChewSwallowProblem() {
		return chewSwallowProblem;
	}

	public void setChewSwallowProblem(enumChewSwallowProblem [] chewSwallowProblem) {
		this.chewSwallowProblem = chewSwallowProblem;
	}
        
        public enumAlimentaryProblem [] getAlimentaryProblem() {
		return alimentaryProblem;
	}

	public void setAlimentaryProblem(enumAlimentaryProblem [] alimentaryProblem) {
		this.alimentaryProblem = alimentaryProblem;
	}
        
        public enumEatProblem [] getEatProblem() {
		return eatProblem;
	}

	public void setEatProblem(enumEatProblem [] eatProblem) {
		this.eatProblem = eatProblem;
	}
        
        public enumFoodAccessibility getFoodAccessibility() {
		return foodAccessibility;
	}

	public void setFoodAccessibility(enumFoodAccessibility foodAccessibility) {
		this.foodAccessibility = foodAccessibility;
	}
        
        public enumDisease [] getDisease() {
		return disease;
	}

	public void setDisease(enumDisease [] disease) {
		this.disease = disease;
	}
        
        public int getScoreItem2() {
		return scoreItem2;
	}

	public void setScoreItem2(int scoreItem2) {
		this.scoreItem2 = scoreItem2;
	}
        
        public int getScoreItem3() {
		return scoreItem3;
	}

	public void setScoreItem3(int scoreItem3) {
		this.scoreItem3 = scoreItem3;
	}
        
        public int getScoreItem4() {
		return scoreItem4;
	}

	public void setScoreItem4(int scoreItem4) {
		this.scoreItem4 = scoreItem4;
	}
        
        public int getScoreItem5() {
		return scoreItem5;
	}

	public void setScoreItem5(int scoreItem5) {
		this.scoreItem5 = scoreItem5;
	}
        
        public int getScoreItem6() {
		return scoreItem6;
	}

	public void setScoreItem6(int scoreItem6) {
		this.scoreItem6 = scoreItem6;
	}
        
        public int getScoreItem7() {
		return scoreItem7;
	}

	public void setScoreItem7(int scoreItem7) {
		this.scoreItem7 = scoreItem7;
	}
        
        public int getScoreItem8() {
		return scoreItem8;
	}

	public void setScoreItem8(int scoreItem8) {
		this.scoreItem8 = scoreItem8;
	}
        
        public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
        	
}
