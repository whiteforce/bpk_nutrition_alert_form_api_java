package com.bpkhospital.nutritionscreen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NutritionscreenApplication {

	public static void main(String[] args) {
		SpringApplication.run(NutritionscreenApplication.class, args);
	}
}
